import telebot

import diff_hosts
import security_info as sec

bot = telebot.TeleBot(sec.TOKEN)


@bot.message_handler(commands=['start'])
def start(message):
    bot.send_message(message.chat.id, "Hello, I'm the findVideoBot. I can find some video for you.")


@bot.message_handler(commands=['help'])
def help(message):
    bot.send_message(message.chat.id, "Are you need help?\n/find - searching 5 links of video\n/add - adding one links")


@bot.message_handler(commands=['find'])
def add(message):
    query = bot.send_message(message.chat.id, 'What you want find?')
    bot.register_next_step_handler(query, get_video)


@bot.message_handler(commands=['add'])
def add(message):
    bot.send_message(message.chat.id, get_video_else())


def get_video(message):
    links = diff_hosts.get_from_youtube(message.text)
    for link in links:
        bot.send_message(message.chat.id, link)


def get_video_else():
    return diff_hosts.get_one_link()


bot.polling(none_stop=True)
