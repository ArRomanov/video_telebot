import urllib.request, urllib.parse, urllib
import re
import urllib
import diff_hosts

search_results = []
main_link = ''


def get_from_youtube(query):
    query_link = urllib.parse.urlencode({"search_query": query})
    content = urllib.request.urlopen("https://www.youtube.com/results?" + query_link)
    diff_hosts.search_results = re.findall('href=\"\/watch\?v=(.*?)\"', content.read().decode())
    diff_hosts.main_link = "https://www.youtube.com/watch?v="
    return get_five_links()


def get_five_links():
    if len(search_results) > 0:
        iterator = 0
        links = []
        print(len(search_results))
        while (iterator < 10):
            links.append(main_link + search_results[iterator])
            del search_results[iterator]
            del search_results[iterator + 1]
            iterator = iterator + 2
        print(len(search_results))
        return links
    else:
        return 'Пустой список видео'


def get_one_link():
    if len(search_results) > 0:
        return main_link + search_results.pop(0)
    else:
        return 'Пустой список видео'
